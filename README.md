** Wake on Lan **

This is a project i've been thinking of doing for quite some time. 
The basic idea was to use some kind of device in the local network to power on my pc when it's turned off/sleeping, 
so using another computer or a phone for example.
I found a youtube video by Brian Lough on how to use an ESP8266 microcontroller and a Telegram Bot to power on/off the computer, which led me to do my own implementation of the said project. 

** Progress **

Currently planning the project, would need to order the microcontroller and other necessary elements.

I have an ESP32 microcontroller and will try to use that.

Currently plannign on to finish the project during my christmas break


** EDIT **

Seems like my esp32 might be broken, so i ordered a esp8266 module, as that seems to be more compatible with WOL in general

** FINAL **

Esp8266 module worked like it was supposed to. Project now finished

Link to Brian Lough's video: https://www.youtube.com/watch?v=ffi9ZZWiHgY

All credits to
Brian Lough
    YouTube: https://www.youtube.com/brianlough
    Tindie: https://www.tindie.com/stores/brianlough/
    Twitter: https://twitter.com/witnessmenow
